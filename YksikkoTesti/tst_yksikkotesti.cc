#include <QString>
#include <QtTest>
#include "statistics.hh"

class YksikkoTesti : public QObject
{
    Q_OBJECT

public:
    YksikkoTesti();

private Q_SLOTS:
    void testitapaus1();
    void bussinLisaysPoistoJaTuho();
    void matkustajienLisaysJaKuolo();
};

YksikkoTesti::YksikkoTesti()
{
}

void YksikkoTesti::testitapaus1()
{
    OmaPuoli::statistics stats;
    QVERIFY2(stats.annaPisteet() >= 0, "Failure");
}
void YksikkoTesti::bussinLisaysPoistoJaTuho()
{
    OmaPuoli::statistics stats;
    stats.uusiNysse();
    QVERIFY2(stats.getBuses() == 1u, "Failure");
    stats.uusiNysse();
    QVERIFY2(stats.getBuses() == 2u, "Failure");
    stats.nysseTuhoutui();
    QVERIFY2(stats.getDestroyed() == 1u, "Failure");
    QVERIFY2(stats.getBuses() == 1u, "Failure");
    stats.nyssePoistui();
    QVERIFY2(stats.getBuses() == 0u, "Failure");
}
void YksikkoTesti::matkustajienLisaysJaKuolo()
{
    OmaPuoli::statistics stats;
    stats.lisaaMatkustajia(2);
    QVERIFY2(stats.getPassengers() == 2u, "Failure");
    stats.matkustajiaKuoli(1);
    QVERIFY2(stats.getPassengers() == 1u, "Failure");
}

QTEST_APPLESS_MAIN(YksikkoTesti)

#include "tst_yksikkotesti.moc"
