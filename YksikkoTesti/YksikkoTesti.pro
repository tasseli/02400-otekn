#-------------------------------------------------
#
# Project created by QtCreator 2015-11-06T09:01:10
#
#-------------------------------------------------

QT       += testlib



TARGET = tst_yksikkotesti
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += C++11

TEMPLATE = app


SOURCES += tst_yksikkotesti.cc \
    ../OmaPuoli/statistics.cc
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../OmaPuoli/statistics.hh

INCLUDEPATH += ../OmaPuoli
DEPENDPATH  += ../OmaPuoli
INCLUDEPATH += ../KurssinPuoli
DEPENDPATH  += ../KurssinPuoli
