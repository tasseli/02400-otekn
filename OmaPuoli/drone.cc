#include "drone.hh"

namespace OmaPuoli
{

OmaPuoli::drone::drone() : destroyed{false}
{
    Rajapinta::Sijainti whereTo;
    whereTo.asetaXY(250,250);
    liiku(whereTo);
}

OmaPuoli::drone::~drone()
{

}

Rajapinta::Sijainti OmaPuoli::drone::annaSijainti() const
{
    return location;
}

void OmaPuoli::drone::liiku(Rajapinta::Sijainti sij)
{
    location = sij;
}

bool OmaPuoli::drone::onkoTuhottu() const
{
    return destroyed;
}

void OmaPuoli::drone::tuhoa()
{
    destroyed = true;
}

}
