#-------------------------------------------------
#
# Project created by QtCreator 2015-11-06T08:59:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OmaPuoli
TEMPLATE = lib
CONFIG += c++11
CONFIG += staticlib

# Lisätty kurssin puolen tavaroiden käyttämiseksi
INCLUDEPATH += $$PWD/../KurssinPuoli
DEPENDPATH += $$PWD/../KurssinPuoli
#LIBS += $$PWD/../KurssinPuoli/sijainti.o


SOURCES += \
    mainwindow.cc \
    statistics.cc \
    city.cc \
    drone.cc \
    luopeli.cc \
    bomb.cc

HEADERS  += \
    mainwindow.hh \
    statistics.hh \
    city.hh \
    drone.hh \
    bomb.hh
FORMS += \
    mainwindow.ui

DISTFILES += \
    ../../../Downloads/bus.jpg \
    ../../../Downloads/mine.png \
    ../../../Downloads/passenger.png \
    bus.jpg \
    passenger.png \
    bomb.png \
    bus.png \
    bus_stop.png \
    person.png

RESOURCES += \
    red.qrc
