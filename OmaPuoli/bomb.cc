#include "bomb.hh"

namespace OmaPuoli{

bomb::bomb()
{

}

bomb::~bomb()
{

}

Rajapinta::Sijainti bomb::annaSijainti() const
{
    return location;
}

void bomb::liiku(Rajapinta::Sijainti sij)
{
    location = sij;
}

bool bomb::onkoTuhottu() const
{
    return destroyed;
}

void bomb::tuhoa()
{
    destroyed = true;
}
}




