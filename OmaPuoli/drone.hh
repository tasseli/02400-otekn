#ifndef DRONE_HH
#define DRONE_HH

/**
  * @file
  * @brief Määrittelee pelaajan lentohärvelin, dronen, luokan.
  */

#include <toimijarp.hh>
#include <QGraphicsPixmapItem>

namespace OmaPuoli
{

class drone : public Rajapinta::ToimijaRP
{

public:
    drone();
    virtual ~drone();

    //ToimijaRP interface

    virtual Rajapinta::Sijainti annaSijainti() const;
    virtual void liiku(Rajapinta::Sijainti sij);
    virtual bool onkoTuhottu() const;
    virtual void tuhoa();

private:
    Rajapinta::Sijainti location;
    bool destroyed;
    QGraphicsPixmapItem _dronepic;
    bool hasBeenDrawn = false;
};

}
#endif // DRONE_HH
