#ifndef STATISTICS_HH
#define STATISTICS_HH
#include <tilastorp.hh>

/**
  * @file
  * @brief Määrittelee statistiikkoja meidän tyyliin
  */

namespace OmaPuoli
{

/**
 * @brief Statistics on...  <3
 *
 * (Jos luokan metodi ei ole poikkeustakuultaan nothrow, voi metodista aina vuotaa ulos poikkeus `std::bad_alloc` (muisti loppui).)
 */
class statistics : public Rajapinta::TilastoRP
{

public:
    /**
      * @brief Rajapintaluokan oletusrakentaja (olemassa dokumentaatiota varten).
      * @post Alussa pistetilasto on nollattu.
      */
    statistics();

    /**
      * @brief Rajapintaluokassa on oletusarvoinen virtuaalipurkaja (olemassa, koska kantaluokalla tulee olla virtuaalipurkaja).
      */
    virtual ~statistics();

    /**
     * @brief annaPisteet palauttaa pelaajan pistemäärän
     * @pre -
     * @return Pisteet
     * @post Poikkeustakuu: nothrow
     */
    virtual int annaPisteet() const;

    /**
     * @brief matkustajiaKuoli kasvattaa tapettujen matkustajien lukumäärää, _dead:ia lkm:llä.
     * @pre lkm > 0
     * @post Poikkeustakuu: vahva
     */
    virtual void matkustajiaKuoli(int lkm);

    /**
     * @brief lisaaMatkustajia kasvattaa _passengers:ia lkm:llä
     * @pre lkm > 0
     * @post Poikkeustakuu: vahva
     */
    virtual void lisaaMatkustajia(int lkm);

    /**
     * @brief nysseTuhoutui ilmoittaa, että nysse tuhoutui
     * @pre -
     * @post Poikkeustakuu: vahva
     */
    virtual void nysseTuhoutui();

    /**
     * @brief uusiNysse ilmoittaa, että peliin on lisätty uusi nysse.
     * @pre -
     * @post Poikkeustakuu: vahva
     */
    virtual void uusiNysse();

    /**
     * @brief nyssePoistui ilmoittaa, että pelistä on poistunut nysse.
     * @pre -
     * @post Poikkeustakuu: vahva
     */
    virtual void nyssePoistui();

    /**
     * @brief getDead palauttaa kentän
     * @return _dead
     * @pre -
     * @post Poikkeustakuu: nothrow
     */
    int getDead ();

    /**
     * @brief getPassengers palauttaa kentän
     * @return _passengers
     * @pre -
     * @post Poikkeustakuu: nothrow
     */
    int getPassengers ();

    /**
     * @brief getBuses palauttaa kentän
     * @return _buses
     * @pre -
     * @post Poikkeustakuu: nothrow
     */
    int getBuses ();

    /**
     * @brief getDestroyed palauttaa kentän
     * @return _destroyed
     * @pre -
     * @post Poikkeustakuu: nothrow
     */
    int getDestroyed ();
    void invariantti () const;

private:
    int _dead=0;
    int _pisteet=0;
    int _passengers=0;
    int _buses=0;
    int _destroyed=0;
};

}

#endif // STATISTICS_HH
