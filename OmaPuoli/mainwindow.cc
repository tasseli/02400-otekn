#include "mainwindow.hh"
#include "ui_mainwindow.h"
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QTime>
#include <QSignalMapper>
#include <string>
#include <map>
#include "city.hh"

using std::string;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QSignalMapper* signalMapper = new QSignalMapper (this);
    ui->setupUi(this);
    connect(ui->quitButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->fireButton, SIGNAL(clicked()), this, SLOT(shoot()));
    connect(_bombTimer, SIGNAL(timeout()), this, SLOT(explodeBomb()));
    connect(ui->leftButton,  SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->rightButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->upButton,    SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->downButton,  SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper -> setMapping (ui->leftButton, "vasen");
    signalMapper -> setMapping (ui->rightButton, "oikea");
    signalMapper -> setMapping (ui->upButton,    "ylos");
    signalMapper -> setMapping (ui->downButton,  "alas");

    connect (signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(moveDrone(const QString &))) ;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addBackground(QImage& backGroundImage)
{
    _clock.start();
    QGraphicsPixmapItem *cityMap = new QGraphicsPixmapItem( QPixmap::fromImage(backGroundImage));
    QGraphicsScene *scene = new QGraphicsScene (this);
    scene->addItem(cityMap);
    ui->graphicsView->setScene(scene);
}

void MainWindow::printPoints(int points)
{
    QString text = "Pisteet: "+ QString::number(points);
    ui->pointsLabel->setText(text);
}

void MainWindow::printBombs(int bombs)
{
    QString text = "Pommit: "+ QString::number(bombs);
    ui->bombLabel->setText(text);
}

void MainWindow::getDronePtr(OmaPuoli::drone *drone)
{
    _drone = drone;
}

void MainWindow::getBombPtr(OmaPuoli::bomb *bomb)
{
    _bomb = bomb;
}

void MainWindow::getCityPtr(OmaPuoli::City *city)
{
    _city = city;
}

void MainWindow::addBusStop(std::shared_ptr<Rajapinta::PysakkiRP> busStop)
{
    if (busStop->annaSijainti().annaX()<500 && busStop->annaSijainti().annaX()>=0 && busStop->annaSijainti().annaY()<500 && busStop->annaSijainti().annaY()>=0 ){
        QPixmap pixmap(":/bus_stop.png");
        QGraphicsPixmapItem *bus_stop = new QGraphicsPixmapItem(pixmap);
        ui->graphicsView->scene()->addItem((bus_stop));
        bus_stop->moveBy(busStop->annaSijainti().annaX(), busStop->annaSijainti().annaY());
    }
}

void MainWindow::moveBus(std::shared_ptr<Rajapinta::ToimijaRP> _bus)
{
    if (_bus->annaSijainti().annaX()<500 && _bus->annaSijainti().annaX()>=0 && _bus->annaSijainti().annaY()<500 && _bus->annaSijainti().annaY()>=0 ){
        Rajapinta::ToimijaRP* actor = _bus.get();
        string nimi = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        if (_graphicsItems.find(nimi) != _graphicsItems.end()){
            _graphicsItems.at(nimi)->setPos(_bus->annaSijainti().annaX(), _bus->annaSijainti().annaY());
        }
    }
}

void MainWindow::removeBus(std::shared_ptr<Rajapinta::ToimijaRP> _bus)
{
    Rajapinta::ToimijaRP* actor = _bus.get();
    string nimi = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
    if (_graphicsItems.find(nimi) != _graphicsItems.end()){
        ui->graphicsView->scene()->removeItem(_graphicsItems.find(nimi)->second);
        _graphicsItems.erase(nimi);
    }

}

void MainWindow::drawDrone(){
    QPixmap pixmap(":/plane.png");
    QGraphicsPixmapItem *drone = new QGraphicsPixmapItem(pixmap);
    _graphicsItems.insert(std::pair<string,QGraphicsPixmapItem*>("drone",drone));
    ui->graphicsView->scene()->addItem(drone);
    Rajapinta::Sijainti whereTo = _drone->annaSijainti();
    _graphicsItems.at("drone")->moveBy(whereTo.annaX(), whereTo.annaY());

}

void MainWindow::drawBomb(Rajapinta::Sijainti location)
{
    ui->fireButton->setEnabled(false);
    location.asetaXY(location.annaX()+12, location.annaY()+40);
    _bomb->liiku(location);
    QPixmap pixmap(":/bomb.png");
    QGraphicsPixmapItem *bomb = new QGraphicsPixmapItem(pixmap);
        _graphicsItems.insert(std::pair<string,QGraphicsPixmapItem*>("bomb",bomb));
    ui->graphicsView->scene()->addItem((bomb));
    _graphicsItems.at("bomb")->moveBy(location.annaX(), location.annaY());
}

void MainWindow::drawPassenger(std::shared_ptr<Rajapinta::ToimijaRP> _passenger)
{
    if (_passenger->annaSijainti().annaX()<500 && _passenger->annaSijainti().annaX()>=0 && _passenger->annaSijainti().annaY()<500 && _passenger->annaSijainti().annaY()>=0 ){
        QPixmap pixmap(":/person.png");
        QGraphicsPixmapItem *passenger = new QGraphicsPixmapItem(pixmap);
        ui->graphicsView->scene()->addItem((passenger));
        passenger->moveBy(_passenger->annaSijainti().annaX(), _passenger->annaSijainti().annaY());
    }
}

void MainWindow::drawBus(std::shared_ptr<Rajapinta::ToimijaRP> _bus)
{
    if (_bus->annaSijainti().annaX()<500 && _bus->annaSijainti().annaX()>=0 && _bus->annaSijainti().annaY()<500 && _bus->annaSijainti().annaY()>=0 ){
        QPixmap pixmap(":/bus.png");
        QGraphicsPixmapItem *bus = new QGraphicsPixmapItem(pixmap);
        Rajapinta::ToimijaRP* actor = _bus.get();
        string nimi = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        _graphicsItems.insert(std::pair<string,QGraphicsPixmapItem*>(nimi,bus));
        ui->graphicsView->scene()->addItem((bus));
        bus->moveBy(_bus->annaSijainti().annaX(), _bus->annaSijainti().annaY());
    }
}

void MainWindow::endGame()
{
    gameEnded = true;
    ui->gameStatusLabel->setText("Peli on loppunut");
}

void MainWindow::moveDrone(const QString & text) {
    Rajapinta::Sijainti locationNow{_drone->annaSijainti()};
    std::pair<int, int> xy;
    if (text == "vasen") {
        xy.first = -10, xy.second = 0;
    }
    else if(text == "oikea") {
        xy.first = 10, xy.second = 0;
    }
    else if(text == "ylos") {
        xy.first = 0, xy.second = -10;
    }
    else if(text == "alas") {
        xy.first = 0, xy.second = 10;
    }
    // TODO: test if legal
    locationNow.asetaXY(locationNow.annaX()+xy.first, locationNow.annaY()+xy.second);
    _drone->liiku(locationNow);
    _graphicsItems.at("drone")->moveBy(xy.first, xy.second);
}


void MainWindow::shoot()
{
    _city->spendBomb();
    drawBomb(_drone->annaSijainti());    
    _bombTimer->setSingleShot(true);
    _bombTimer->start(1000);
}

void MainWindow::explodeBomb()
{
    std::vector<std::shared_ptr<Rajapinta::ToimijaRP>> busesNearby = _city->annaToimijatLahella(_bomb->annaSijainti());
    std::vector<std::shared_ptr<Rajapinta::ToimijaRP>>::iterator it = busesNearby.begin();
    while (it != busesNearby.end()){
        _city->toimijaTuhottu(*it);
        it++;
    }
    if (_graphicsItems.find("bomb")!= _graphicsItems.end()){
        ui->graphicsView->scene()->removeItem(_graphicsItems.find("bomb")->second);
        _graphicsItems.erase("bomb");
    }
    if (!gameEnded){
        ui->fireButton->setEnabled(true);
    }
}

void accio_resource() { Q_INIT_RESOURCE(red);}
