#ifndef CITY_HH
#define CITY_HH

/**
  * @file
  * @brief Määrittelee kaupunkiluokan.
  */

#include "kaupunkirp.hh"
#include "pysakkirp.hh"
#include "toimijarp.hh"

#include "mainwindow.hh"

#include <iostream>
#include <drone.hh>
#include <bomb.hh>
#include <statistics.hh>

#include <QTime>
#include <memory>
#include <vector>
#include <QImage>

namespace OmaPuoli
{

class City : public Rajapinta::KaupunkiRP
{

public:
    City();
    virtual ~City();
    // KaupunkiRP interface

    virtual void asetaTausta(QImage& perustaustakuva, QImage& isotaustakuva);
    virtual void asetaKello(QTime kello);
    virtual void lisaaPysakki(std::shared_ptr<Rajapinta::PysakkiRP> pysakki);
    virtual void peliAlkaa();
    virtual void lisaaToimija(std::shared_ptr<Rajapinta::ToimijaRP> uusitoimija);
    virtual void poistaToimija(std::shared_ptr<Rajapinta::ToimijaRP> toimija);
    virtual void toimijaTuhottu(std::shared_ptr<Rajapinta::ToimijaRP> toimija);
    virtual bool loytyykoToimija(std::shared_ptr<Rajapinta::ToimijaRP> toimija) const;
    virtual void toimijaLiikkunut(std::shared_ptr<Rajapinta::ToimijaRP> toimija);
    virtual std::vector<std::shared_ptr<Rajapinta::ToimijaRP>> annaToimijatLahella(Rajapinta::Sijainti paikka) const;
    virtual bool peliLoppunut() const;
    bool areLocationsCloseToEachOther (Rajapinta::Sijainti& location1, Rajapinta::Sijainti& location2) const;
    void spendBomb ();


private:
    OmaPuoli::drone *_drone = new OmaPuoli::drone;
    OmaPuoli::bomb *_bomb = new OmaPuoli::bomb;
    MainWindow *_m = new MainWindow;
    int _bombs = 3;
    OmaPuoli::statistics _statistics;
    std::map<string,std::shared_ptr<Rajapinta::PysakkiRP>> _busStops;
    std::map<string,std::shared_ptr<Rajapinta::ToimijaRP>> _buses;
    std::map<string,std::shared_ptr<Rajapinta::ToimijaRP>> _passengers;
};

}
/* TODO:

 *
 *
 * - pelin lopetus esim. tietyn ajan jälkeen (masterpelikello)
 * - statistiikkojen printtaus näytölle (vähintään pelin lopussa)
 * - yksikkötestit statistiikoille
 *
 * - yleistä siistintää esim. bussit pois kartalta kun ne ajavat ulos karttanäkymästä ja droonin liikkeiden rajoitus vain kartan alueelle
 *
 * + mahdolliset lisäosat jotka oletettavasti helpohkoa toteuttaa:
 * + pisteiden livepäivitys
 * + isompi tai nopeammin räjähtävä pommi tietyn pistemäärän jälkeen uudesta napista painamalla (hakeutuva pommi?)
 * + kartan keskittäminen drooniin ja isompi kartta
 * + droonin tasainen liike (tämähän me toteutettiin jo kerran vahingossa :) )
 */


#endif // CITY_HH
