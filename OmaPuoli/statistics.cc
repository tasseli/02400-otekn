#include "statistics.hh"
#include "QtDebug"

OmaPuoli::statistics::statistics()
{
    invariantti ();
}


OmaPuoli::statistics::~statistics()
{

}

int OmaPuoli::statistics::annaPisteet() const
{
    invariantti ();
    return _pisteet;
}

void OmaPuoli::statistics::matkustajiaKuoli(int lkm)
{
    invariantti ();
    if(lkm > 0 && _passengers >= lkm) {
        _passengers -= lkm;
        _dead += lkm;
        _pisteet += 1;
    }
    invariantti ();
}

void OmaPuoli::statistics::lisaaMatkustajia(int lkm)
{
    invariantti ();
    if(lkm > 0)
        _passengers += lkm;
    invariantti ();
}

void OmaPuoli::statistics::nysseTuhoutui()
{
    invariantti ();
    if(_buses > 0) {
        _buses--;
        _destroyed++;
        _pisteet += 3;
    }
    invariantti ();
}

void OmaPuoli::statistics::uusiNysse()
{
    invariantti ();
    _buses++;
    invariantti ();
}

void OmaPuoli::statistics::nyssePoistui()
{
    invariantti ();
    if(_buses > 0)
        _buses--;
    invariantti ();
}

int OmaPuoli::statistics::getDead()
{
    return _dead;
}

int OmaPuoli::statistics::getPassengers()
{
    return _passengers;
}

int OmaPuoli::statistics::getBuses()
{
    return _buses;
}

int OmaPuoli::statistics::getDestroyed()
{
    return _destroyed;
}

void OmaPuoli::statistics::invariantti() const
{
    Q_ASSERT(_pisteet >= 0);
    Q_ASSERT(_destroyed >= 0);
    Q_ASSERT(_dead >= 0);
    Q_ASSERT(_buses >= 0);
    Q_ASSERT(_passengers >= 0);
}
