#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

/**
  * @file
  * @brief Määrittelee pääikkunan jossa peli näytetään.
  */

#include <QMainWindow>
#include <QGraphicsView>
#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QTime>
#include <QTimer>
#include <drone.hh>
#include <bomb.hh>

namespace OmaPuoli{
    class City;
}

#include <pysakkirp.hh>
#include <string>
#include <map>

using std::string;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addBackground (QImage& backGroundImage);
    void printPoints (int points);
    void printBombs (int bombs);
    void getDronePtr (OmaPuoli::drone *drone);
    void getBombPtr (OmaPuoli::bomb *bomb);
    void getCityPtr (OmaPuoli::City *city);
    void addBusStop (std::shared_ptr<Rajapinta::PysakkiRP> busStop);
    void moveBus (std::shared_ptr<Rajapinta::ToimijaRP> _bus);
    void removeBus (std::shared_ptr<Rajapinta::ToimijaRP> _bus);
    void drawDrone ();
    void drawBomb(Rajapinta::Sijainti location);
    void drawPassenger (std::shared_ptr<Rajapinta::ToimijaRP> _passenger);
    void drawBus (std::shared_ptr<Rajapinta::ToimijaRP> _bus);
    void endGame ();

private:
    Ui::MainWindow *ui;
    QTime _clock;
    QTimer *_bombTimer = new QTimer(this);
    QTimer *_busUpdateTimer = new QTimer(this);
    OmaPuoli::drone *_drone = nullptr;
    OmaPuoli::bomb *_bomb = nullptr;
    OmaPuoli::City *_city = nullptr;
    std::map<string,QGraphicsPixmapItem*> _graphicsItems;
    bool gameEnded = false;

public slots:


    void moveDrone(const QString & text);
    void shoot();
    void explodeBomb();
};

#endif // MAINWINDOW_HH
