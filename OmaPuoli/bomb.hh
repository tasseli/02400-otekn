#ifndef BOMB_H
#define BOMB_H

/**
  * @file
  * @brief Määrittelee ainoan käyttämämme aseen, pommin, luokkana.
  */

#include <toimijarp.hh>
#include <QGraphicsPixmapItem>

namespace OmaPuoli{

class bomb : public Rajapinta::ToimijaRP
{
public:
    bomb();
    virtual ~bomb();

    //ToimijaRP interface

    virtual Rajapinta::Sijainti annaSijainti() const;
    virtual void liiku(Rajapinta::Sijainti sij);
    virtual bool onkoTuhottu() const;
    virtual void tuhoa();
private:
    Rajapinta::Sijainti location;
    bool destroyed;
    QGraphicsPixmapItem _bombPic;
};
}

#endif // BOMB_H
