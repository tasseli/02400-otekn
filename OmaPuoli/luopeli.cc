
#include "luopeli.hh"
#include "city.hh"
#include "mainwindow.hh"
#include <iostream>

namespace Rajapinta
{

std::shared_ptr<KaupunkiRP> luoPeli()
{
    OmaPuoli::City k;
    std::shared_ptr<Rajapinta::KaupunkiRP> c = std::make_shared<OmaPuoli::City>(k);
    return c;
}

}
