#include "city.hh"
#include <QGraphicsPixmapItem>
#include "mainwindow.hh"

namespace OmaPuoli
{

City::City()
{

}

City::~City()
{

}

void City::asetaTausta(QImage &perustaustakuva, QImage &isotaustakuva)
{
    _m->getDronePtr(_drone);
    _m->getBombPtr(_bomb);
    _m->getCityPtr(this);
    _m->addBackground(perustaustakuva);
    _m->show();
}

void City::asetaKello(QTime kello)
{
    //tämän koimme turhaksi
}

void City::lisaaPysakki(std::shared_ptr<Rajapinta::PysakkiRP> pysakki)
{
    _m->addBusStop(pysakki);
}

void City::peliAlkaa()
{
    _m->drawDrone();
    _m->printPoints(_statistics.annaPisteet());
    _m->printBombs(_bombs);
}

void City::lisaaToimija(std::shared_ptr<Rajapinta::ToimijaRP> uusitoimija)
{
    Rajapinta::ToimijaRP* actor = uusitoimija.get();
    if (dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)){
        string name = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        _m->drawBus(uusitoimija);
        _buses.insert(std::pair<string, std::shared_ptr<Rajapinta::ToimijaRP>>(name, uusitoimija));
        _statistics.uusiNysse();
    } else {       
        _statistics.lisaaMatkustajia(1);
        //matkustajia ei piirretä, koska vaikuttavat toistaiseksi liikkuvan vain busseilla eikä itsenäisesti
        //_m->drawPassenger(uusitoimija);
        //_passengers.insert(std::pair<string, std::shared_ptr<Rajapinta::ToimijaRP>>(nimi, uusitoimija));
    }
}

void City::poistaToimija(std::shared_ptr<Rajapinta::ToimijaRP> toimija)
{
    Rajapinta::ToimijaRP* actor = toimija.get();
    if (dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)){
        string name = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        _m->removeBus(toimija);
        _buses.erase(name);
        _statistics.nyssePoistui();
    }
}

void City::toimijaTuhottu(std::shared_ptr<Rajapinta::ToimijaRP> toimija)
{
    Rajapinta::ToimijaRP* actor = toimija.get();
    if (dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)){
        string name = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        _m->removeBus(toimija);
        _buses.at(name)->tuhoa();
        _statistics.nysseTuhoutui();
        _buses.erase(name);
        _m->printPoints(_statistics.annaPisteet());
    } else {
        _statistics.matkustajiaKuoli(1);
    }
}

bool City::loytyykoToimija(std::shared_ptr<Rajapinta::ToimijaRP> toimija) const
{
    Rajapinta::ToimijaRP* actor = toimija.get();
    if (dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)){
         string nimi = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        if (_buses.find(nimi) != _buses.end()){
            return true;
        }
    } else {
        return true;
    }
}

void City::toimijaLiikkunut(std::shared_ptr<Rajapinta::ToimijaRP> toimija)
{
    Rajapinta::ToimijaRP* actor = toimija.get();
    if (dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)){
        string nimi = dynamic_cast<Rajapinta::KulkuneuvoRP*>(actor)->annaNimi();
        _m->moveBus(toimija);
    }
}

std::vector<std::shared_ptr<Rajapinta::ToimijaRP> > City::annaToimijatLahella(Rajapinta::Sijainti paikka) const
{
    std::vector<std::shared_ptr<Rajapinta::ToimijaRP>> actorsNearby;
    std::map <string, std::shared_ptr<Rajapinta::ToimijaRP>>::const_iterator it = _buses.begin();
    while (it != _buses.end())
    {
        Rajapinta::Sijainti location1 = it->second->annaSijainti();
        if (areLocationsCloseToEachOther(location1, paikka)){
           actorsNearby.push_back(it->second);
        }
        it++;
    }
    return actorsNearby;
}

bool City::peliLoppunut() const
{
    if (_bombs == 0){
        _m->endGame();
        return true;
    } else {
        return false;
    }
}

bool City::areLocationsCloseToEachOther(Rajapinta::Sijainti &location1, Rajapinta::Sijainti &location2) const
{
    int distance{80};
    if (location1.annaX() < location2.annaX()+distance && location1.annaX() > location2.annaX()-distance){
        if (location1.annaY() < location2.annaY()+distance && location1.annaY() > location2.annaY()-distance){
            return true;
        }
    }
    else
        return false;
}

void City::spendBomb()
{
    if (_bombs >0){
        _bombs -= 1;
    }
    _m->printBombs(_bombs);
}
}
