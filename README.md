# README #

Our two-student cooperation game project with Antti Altsten on Tampere University of Technology's course 02400, Programming Techniques. First part of the project, the specifications and interfaces, were built by assistant teachers on first half of the course. On the latter half we cooperated to create a game according to documentation, satisfying the requirements of the specs, also using Tampere Public Transit's open data API.
